function validateMessDate(elem) {
    let today = new Date();
    today.setHours(0, 0, 0, 0);

    let measurementDate = new Date(elem.value);
    measurementDate.setHours(0, 0, 0, 0);

    if (measurementDate <= today) {
        elem.classList.add("is-valid");
        elem.classList.remove("is-invalid");
    } else {
        elem.classList.add("is-invalid");
        elem.classList.remove("is-valid");
    }
}