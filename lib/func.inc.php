<?php

$error = [];

function validate($name, $messDate, $height, $weight) {
    return validateName($name) & validateMessDate($messDate) & validateHeight($height) & validateWeight($weight);
}

function validateName($name)
{
    global $error;

    if (strlen($name) == 0) {
        $error['name'] = "Name darf nicht leer sein!";
        return false;
    } else if (strlen($name) > 25) {
        $error['name'] = "Name darf nicht mehr als 25 Zeichen haben!";
        return false;
    } else {
        return true;
    }

}

function validateMessDate($messDate)
{
    global $error;

    try {
        if ($messDate == "") {
            $error['messDate'] = "Datum darf nicht leer sein!";
            return false;
        } else if (new DateTime($messDate) > new DateTime()) {
            $error['messDate'] = "Das Datum darf nicht in der Zukunft liegen!";
            return false;
        } else {
            return true;
        }
    } catch (Exception $exception) {
        $error['messDate'] = "Messdatum ungültig";
        return false;
    }
}


    function validateHeight($height)
    {
        global $error;

        if (!is_numeric($height) || $height > 260 || $height < 40) {
            $error['height'] = "Größe ist ungültig.";
            return false;
        } else {
            return true;
        }
    }

    function validateWeight($weigth)
    {
        global $error;

        if (!is_numeric($weigth) || $weigth > 300 || $weigth < 5) {
            $error['height'] = "Gewicht ist ungültig.";
            return false;
        } else {
            return true;
        }
    }

function calculateBMI($height, $weight){
    $BMI = round($weight/pow($height/100, 2), 1);
    return $BMI;
}

?>